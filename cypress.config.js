const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "hthn59",
  downloadsFolder: "cypress/downloads",
  screenshotsFolder: "cypress/reports/screenshots",
  videosFolder: "cypress/videos",
  viewportHeight: 1000,
  viewportWidth: 1280,
  numTestsKeptInMemory: 10,
  failOnStatusCode: false,
  screenshotOnRunFailure: true,
  watchForFileChanges: true,
  video: true,
  retries: {
    runMode: 2,
  },
  e2e: {
    baseUrl: "https://www.airtable.com/",
    includeShadowDom: true,
    defaultCommandTimeout: 20000,
    execTimeout: 40000,
    pageLoadTimeout: 20000,
    requestTimeout: 10000,
    responseTimeout: 10000,
    specPattern: "cypress/e2e/**/*.cy.js",
  },
});
