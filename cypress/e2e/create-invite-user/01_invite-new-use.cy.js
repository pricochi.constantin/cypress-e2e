/* eslint-disable */
/// <reference types="cypress" />
/* eslint-enable */

import { users } from "../../fixtures/users.json";
/* Generate random email */
const email = "exercise" + Cypress._.random(0, 1e4).toString() + "@test.com";
/* Generate random Name */
const name = "Test" + " " + "User" + Cypress._.random(0, 1e2).toString();
/* Generate random password */
const password = Cypress._.random(0, 1e9).toString() + "@T";
/* Generate random email */
const emailInviteUser =
  "invite" + Cypress._.random(0, 1e4).toString() + "@test.com";

describe("Invite New User", () => {
  it("Create New User", () => {
    cy.visit("/");

    /* Fill in the Sing Up Form */
    cy.createNewUser(email, name, password);

    /* Set up the user team */
    cy.setUpNewUserTeam();

    /* Verify the new user has successfully signed up */
    cy.contains(
      "span",
      "Please verify your email address by clicking the link sent to"
    ).should("be.visible");

    /* Save the Credentials of the New Created User */
    cy.writeFile("cypress/fixtures/users.json", {
      name: name,
      email: email,
      password: password,
    });
  });

  it("Create New Base - Share the Base with New user", () => {
    /* User Logs in */
    cy.login("email", "password");
    cy.readFile("cypress/fixtures/users.json").then((user) => {
      cy.contains("span", user.email).should("be.visible");
    });

    /* Create New Base */
    cy.addBase();
    cy.go("back");

    /* Share the Base */
    cy.shareTheBase(emailInviteUser);

    /* Verify the newly added collaborator’s email is shown under “Base Collaborators” */
    cy.get(`[title="${emailInviteUser}"]`)
      .findByText(emailInviteUser)
      .should("exist");

    /* Verify the collaborator has the “Editor” role shown under “Base Collaborators” */
    cy.get("[class='py1 mt-half flex items-center'] [class*=selectMenuButton]")
      .findByText("Editor")
      .should("exist");
  });
});
