import "./commands/common-commands/common-commands";
import "./commands/base/commands-base";
import "./commands/invite-user/commands-invite-user";
import "./commands/new-user/commands-new-user";
import "./commands/sing-in/commands-sign-in";
import "@testing-library/cypress/add-commands";
import { configure } from "@testing-library/cypress";
import "cypress-file-upload";

/* Configure Test Id Attribute */
configure({ testIdAttribute: "id" });

Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});
