Cypress.Commands.add("addBase", () => {
  cy.findByTitle("Start from scratch").should("be.visible").click();
  cy.findByTestId("appTopbar").findByText("Untitled Base").should("be.visible");
});
