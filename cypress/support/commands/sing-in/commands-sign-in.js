import { users } from "../../../fixtures/users.json";

Cypress.Commands.add("login", (email, password) => {
  cy.visit("/");
  cy.get("nav").findByText("Sign in").should("exist").click();
  cy.readFile("cypress/fixtures/users.json").then((user) => {
    cy.findByTestId("ssrInnerContainer")
      .should("be.visible")
      .within(() => {
        cy.findByTestId("emailLogin")
          .should("exist")
          .type(user.email)
          .should("have.value", user.email);
      });
    cy.findByTestId("ssrInnerContainer").then(($body) => {
      if ($body.text().includes("Continue")) {
        cy.get("button").findByText("Continue").should("be.visible").click();
        cy.findByTestId("passwordLogin")
          .should("be.visible")
          .type(user.password)
          .should("have.value", user.password);
        cy.get("button").findByText("Sign in").should("be.visible").click();
      } else {
        cy.findByTestId("passwordLogin")
          .should("be.visible")
          .type(user.password)
          .should("have.value", user.password);
        cy.get("button").findByText("Sign in").should("be.visible").click();
      }
    });
    cy.location("hostname").should("equal", "airtable.com");
    cy.get("h2").findByText("My First Workspace").should("exist");
  });
});
