Cypress.Commands.add("createNewUser", (email, name, password) => {
  cy.get("nav").findByText("Sign up for free").should("exist").click();
  cy.findAllByTestId("multiStepSignupForm").within(() => {
    cy.get("h1").findByText("Create your free account").should("be.visible");
    cy.findAllByTestId("emailSignup")
      .should("be.visible")
      .type(email)
      .should("have.value", email);
    cy.findByRole("button", { name: /Continue/i })
      .should("exist")
      .and("be.enabled")
      .click();
    cy.findByTestId("fullName")
      .should("be.visible")
      .type(name)
      .should("have.value", name);
    cy.findByTestId("password")
      .should("be.visible")
      .type(password)
      .should("have.value", password);
    cy.findByRole("button", { name: /Continue/i })
      .should("exist")
      .and("be.enabled")
      .click();
  });
});

Cypress.Commands.add("setUpNewUserTeam", () => {
  cy.findAllByLabelText("Engineering").should("exist").click({ force: true });
  cy.findByText("Continue").should("exist").click();
  cy.findByText("Skip").should("exist").click();
  cy.findByText("Go to workspace").should("exist").click();
});
