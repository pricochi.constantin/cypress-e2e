// similar functions to findByTestId("id")
// we can create custom command to find the id locators
Cypress.Commands.add("getById", (input) => {
  cy.get(`[id=${input}]`);
});
