Cypress.Commands.add("shareTheBase", (email) => {
  cy.findByText("Share").should("exist").click();
  cy.get('[placeholder="Invite more workspace collaborators via email"]')
    .should("exist")
    .type(email)
    .should("have.value", email);
  cy.get("[class*=selectMenuButton]")
    .findByText("Creator")
    .should("exist")
    .click();
  cy.get("[role*=menuitem]>div").eq(4).should("exist").click();
  cy.get("[class*=selectMenuButton]").findByText("Editor").should("exist");
  cy.findByText("Send invite").should("be.visible").click();
});
